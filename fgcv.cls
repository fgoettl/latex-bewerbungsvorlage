\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{fgcv}

%-----------------------------------------------------------------------------
%	BaseClass
%-----------------------------------------------------------------------------
\LoadClass{article}

%-----------------------------------------------------------------------------
%	Options
%-----------------------------------------------------------------------------
\DeclareOption{entwurf}{\AtEndOfClass{
		\RequirePackage{draftwatermark}
		\definecolor{pink}{rgb}{0.95,0.9,0.95}
		\SetWatermarkText{\textsf{\textcolor{pink}{ENTWURF}}}
		\SetWatermarkScale{10}
}}
\DeclareOption*{\PassOptionsToClass{\CurrentOption}{article}}
\ProcessOptions\relax

%-----------------------------------------------------------------------------
%	Packages
%-----------------------------------------------------------------------------
\RequirePackage[T1]{fontenc}
\RequirePackage{tikz}
\usetikzlibrary{calc}
\RequirePackage{ifthen}
\RequirePackage[absolute,overlay]{textpos}
\RequirePackage{tabularray}
\RequirePackage{fontawesome}
\RequirePackage{multicol}
\RequirePackage{fancyhdr}
\RequirePackage{xcolor}
\RequirePackage{hyperref}


%-----------------------------------------------------------------------------
%	Colors
%-----------------------------------------------------------------------------
\definecolor{mathgreen}{RGB}{0,101,97}
\definecolor{lightgray}{RGB}{200,200,200}
\definecolor{midgray}{RGB}{100,100,100}
\definecolor{almostblack}{RGB}{20,20,20}
\definecolor{almostwhite}{RGB}{250,250,250}
\definecolor{white}{RGB}{255,255,255}
\definecolor{snow}{rgb}{1.0, 0.98, 0.98}
\definecolor{gray}{HTML}{4D4D4D}
\definecolor{sidecolor}{HTML}{E7E7E7}
\definecolor{mainblue}{HTML}{0E5484}
\definecolor{maingray}{HTML}{B9B9B9}

\colorlet{lightcolor}{sidecolor}
\colorlet{darkcolor}{black}
\colorlet{accentcolor}{blue!70!black}

\colorlet{sidebarcolor}{lightcolor}
\colorlet{negativetexttextcolor}{snow}
\colorlet{negativetextsurroundingcolor}{darkcolor}
\colorlet{profilesectiontitlecolor}{accentcolor}
\colorlet{listitemcolor}{accentcolor}
\colorlet{pagenumberbackgroundcolor}{lightcolor}
\colorlet{extrapageentrycolor}{accentcolor}
\colorlet{linkcolor}{accentcolor}

%-----------------------------------------------------------------------------
%	Dimensions
%-----------------------------------------------------------------------------
% Settings
\newlength{\sidebarwidth}
\setlength{\sidebarwidth}{7cm}
\newlength{\sidebartopmargin}
\setlength{\sidebartopmargin}{0.3cm}
\newlength{\sidebarsidemargin}
\setlength{\sidebarsidemargin}{0.5cm}
\newlength{\profilepicturemargin}
\setlength{\profilepicturemargin}{0.5cm}
\newlength{\profilepicturemaxheight}
\setlength{\profilepicturemaxheight}{14cm}
\newlength{\personaldataverticalspacing}
\setlength{\personaldataverticalspacing}{0.9cm}

\newlength{\contenttopmargin}
\setlength{\contenttopmargin}{0.5cm}
\newlength{\contentleftmargin}
\setlength{\contentleftmargin}{0.3cm}
\newlength{\contentrightmargin}
\setlength{\contentrightmargin}{0.6cm}
\newlength{\listitemskip}
\setlength{\listitemskip}{0.5cm}
\newlength{\contentlistskip}
\setlength{\contentlistskip}{2cm}

\newlength{\signaturewidth}
\setlength{\signaturewidth}{5cm}
\newlength{\signatureheight}
\setlength{\signatureheight}{1.5cm}

\newlength{\lettertopmargin}
\setlength{\lettertopmargin}{0.2cm}
\newlength{\letterleftmargin}
\setlength{\letterleftmargin}{2.2cm}
\newlength{\letterrightmargin}
\setlength{\letterrightmargin}{2.2cm}
\newlength{\letterheadskip}
\setlength{\letterheadskip}{2cm}
\newlength{\letterrecipientskip}
\setlength{\letterrecipientskip}{1cm}
\newlength{\lettersubjectskip}
\setlength{\lettersubjectskip}{1cm}
\newlength{\letterbodyskip}
\setlength{\letterbodyskip}{0cm}

\newlength{\pagenumbermarin}
\setlength{\pagenumbermarin}{0.5cm}

\newlength{\bottommargin}
\setlength{\bottommargin}{2cm}

\newlength{\extrapagecolumnsep}
\setlength{\extrapagecolumnsep}{2cm}

% Dependent
\newlength{\sidebartextwidth}
\setlength{\sidebartextwidth}{\dimexpr(\sidebarwidth-2\sidebarsidemargin)\relax}
\newlength{\profilepicturewidth}
\setlength{\profilepicturewidth}{\dimexpr(\sidebartextwidth-2\profilepicturemargin)\relax}
\newlength{\contenttextwidth}
\newlength{\contentabsoluteleftmargin}
\setlength{\contentabsoluteleftmargin}{\dimexpr(\sidebarwidth+\contentleftmargin)\relax}

\newlength{\letterwidth}
\newlength{\letterheadheight}
\setlength{\letterheadheight}{\dimexpr(0cm-\letterheadskip)\relax}
\newlength{\letterrecipientheight}
\setlength{\letterrecipientheight}{\dimexpr(0cm-\letterrecipientskip)\relax}
\newlength{\subjectlineheight}
\setlength{\subjectlineheight}{\dimexpr(0cm-\lettersubjectskip)\relax}
\newlength{\letterbodytopmargin}
\newlength{\lettersubjectlinetopmargin}
\newlength{\letterrecipienttopmargin}

%-----------------------------------------------------------------------------
%	Geometry and Numbering
%-----------------------------------------------------------------------------
\RequirePackage[a4paper,left=\letterleftmargin, right=\letterrightmargin, bottom=\bottommargin]{geometry}
\pagestyle{fancy}
\fancyhf{}
\renewcommand{\headrulewidth}{0pt}
\fancyfoot[OR]{\colorbox{pagenumberbackgroundcolor}{Seite \thepage}}
\newlength{\extrafootwidth}
\setlength{\extrafootwidth}{\dimexpr(\letterrightmargin-\pagenumbermarin)\relax}
\addtolength{\headwidth}{\extrafootwidth}


%-----------------------------------------------------------------------------
%	Textstyle
%-----------------------------------------------------------------------------
\RequirePackage{parskip}
\RequirePackage[sfdefault]{ClearSans}
\renewcommand{\familydefault}{\sfdefault}
\newcommand{\negative}[1]{\colorbox{negativetextsurroundingcolor}{{\textcolor{negativetexttextcolor}{{#1}}}}}
\hypersetup{
	colorlinks=true,  
	urlcolor=linkcolor,
	linkcolor=darkcolor,
}

%-----------------------------------------------------------------------------
%	Datafields
%-----------------------------------------------------------------------------
\newcommand{\cvbirthdate}[1]{\renewcommand{\cvbirthdate}{#1}}
\newcommand{\cvmail}[1]{\renewcommand{\cvmail}{#1}}
\newcommand{\cvphonenumber}[1]{\renewcommand{\cvphonenumber}{#1}}
\newcommand{\cvaddressa}[1]{\renewcommand{\cvaddressa}{#1}}
\newcommand{\cvaddressb}[1]{\renewcommand{\cvaddressb}{#1}}
\newcommand{\cvprofile}[1]{\renewcommand{\cvprofile}{#1}}
\newcommand{\cvprofilepic}[1]{\renewcommand{\cvprofilepic}{#1}}
\newcommand{\cvsignature}[1]{\renewcommand{\cvsignature}{#1}}
\newcommand{\cvfirstname}[1]{\renewcommand{\cvfirstname}{#1}}
\newcommand{\cvlastname}[1]{\renewcommand{\cvlastname}{#1}}
\newcommand{\cvcontent}[1]{\renewcommand{\cvcontent}{#1}}
\newcommand{\cvdate}[1]{\renewcommand{\cvdate}{#1}}

\newcommand{\letterrecipientname}[1]{\renewcommand{\letterrecipientname}{#1}}
\newcommand{\letterrecipientaddressa}[1]{\renewcommand{\letterrecipientaddressa}{#1}}
\newcommand{\letterrecipientaddressb}[1]{\renewcommand{\letterrecipientaddressb}{#1}}
\newcommand{\lettersubject}[1]{\renewcommand{\lettersubject}{#1}}
\newcommand{\lettercontent}[1]{\renewcommand{\lettercontent}{#1}}
\newcommand{\signature}[1]{\renewcommand{\signature}{#1}}

\newcommand{\doifdef}[2]{\ifthenelse{\equal{#1}{}}{}{#2}}

%-----------------------------------------------------------------------------
%	Icons
%-----------------------------------------------------------------------------
\newcommand{\vcenteredhbox}[1]{%
	\begingroup%
	\setbox0=\hbox{#1}\parbox{\wd0}{\box0}%
	\endgroup%
}
\newcommand{\icon}[1]{%
	\vcenteredhbox{\colorbox{negativetextsurroundingcolor}{\makebox(12, 12){\textcolor{negativetexttextcolor}{\large\csname fa#1\endcsname}}}}%
}
\newcommand{\iconwhite}[1]{%
	\vcenteredhbox{\colorbox{white}{\makebox(9, 9){\textcolor{black}{\large\csname fa#1\endcsname}}}}%
}

%-----------------------------------------------------------------------------
%	TOC
%-----------------------------------------------------------------------------
\setcounter{secnumdepth}{0}
\renewcommand{\contentsname}{\negative{\Large\normalfont{}Anhang}}

%-----------------------------------------------------------------------------
%	Including PDFs
%-----------------------------------------------------------------------------
\usepackage{pdfpages}
\newcommand{\pdfwithtocentry}[2]{
	\phantomsection\addcontentsline{toc}{subsection}{#2}
	\includepdf[pages=-, pagecommand={}]{#1}
}

%-----------------------------------------------------------------------------
%	ANSCHREIBEN
%-----------------------------------------------------------------------------
\newcommand{\makeletter}{
	\setlength{\letterwidth}{\dimexpr(\paperwidth-\letterrightmargin-\letterleftmargin)\relax}
	\makeletterhead
	\makerecipientanddate
	\makesubjectline
	\makeletterbody
	\phantom{.}
}

\newcommand{\makeletterhead}{
	\begin{textblock*}{\paperwidth}(0cm, \lettertopmargin)
		\begin{center}
			{\Large\negative{\cvfirstname{} \cvlastname{}}}
			
			\begin{tikzpicture}
				\draw (-6,0) -- (6,0);
				%\filldraw [gray] (0,0) circle (4pt);
			\end{tikzpicture}
			
			{\large\iconwhite{Phone}{\cvphonenumber} | \iconwhite{MapMarker}{\cvaddressa{} / \cvaddressb}%
			
			\iconwhite{Envelope}{\cvmail}}
		\end{center}
	\end{textblock*}%
	% Store the height of the letter head in \letterheadheight.
	\setbox0=\vbox{
		{\Large\negative{\cvfirstname{} \cvlastname{}}}
		
		\begin{tikzpicture}
			\draw (-6,0) -- (6,0);
			%\filldraw [gray] (0,0) circle (4pt);
		\end{tikzpicture}
		
		{\large\iconwhite{Phone}{\cvphonenumber} | \iconwhite{MapMarker}{\cvaddressa{} / \cvaddressb}%
			
			\iconwhite{Envelope}{\cvmail}}}
	\letterheadheight=\ht0 \advance\letterheadheight by \dp0
}

\newcommand{\makerecipientanddate}{
	\setlength{\letterrecipienttopmargin}{\dimexpr(\letterheadheight+\lettertopmargin+\letterheadskip)\relax}
	\begin{textblock*}{\letterwidth}(\letterleftmargin, \letterrecipienttopmargin)
		\begin{flushleft}\large
			\letterrecipientname\\
			\letterrecipientaddressa\\
			\letterrecipientaddressb\hfill\cvdate
		\end{flushleft}
	\end{textblock*}
	% Store height of recipient 
	\setbox0=\vbox{
		\begin{flushleft}
			\letterrecipientname\\
			\letterrecipientaddressa\\
			\letterrecipientaddressb\hfill\cvdate
	\end{flushleft}}
	\letterrecipientheight=\ht0 \advance\letterrecipientheight by \dp0
}

\newcommand{\makesubjectline}{
	\setlength{\lettersubjectlinetopmargin}{\dimexpr(\letterrecipientheight+\letterrecipientskip+\letterheadheight+\lettertopmargin+\letterheadskip)\relax}
	\begin{textblock*}{\letterwidth}(\letterleftmargin, \lettersubjectlinetopmargin)
		\begin{flushleft}
			\large\negative{\lettersubject}
		\end{flushleft}
	\end{textblock*}
	% Store height of subjectline 
	\setbox0=\vbox{
		\begin{flushleft}
			\large\negative{\lettersubject}
	\end{flushleft}}
	\subjectlineheight=\ht0 \advance\subjectlineheight by \dp0
}

\newcommand{\makeletterbody}{
	\setlength{\letterbodytopmargin}{\dimexpr(\letterrecipientheight+\letterrecipientskip+\letterheadheight+\lettertopmargin+\letterheadskip+\lettersubjectskip+\subjectlineheight)\relax}
	\begin{textblock*}{\letterwidth}(\letterleftmargin, \letterbodytopmargin)
		\large\lettercontent
		\vspace{\letterbodyskip}
		\begin{flushleft}%
			\begin{tikzpicture}%
				\node[inner sep=0pt] at (0,0)
				{\includegraphics[width=\signaturewidth, height=\signatureheight, keepaspectratio]{\signature}};
			\end{tikzpicture}
		\end{flushleft}
	\end{textblock*}
}


%-----------------------------------------------------------------------------
%	LEBENSLAUF
%-----------------------------------------------------------------------------

\newcommand{\makecv}{
	\makesidebar
	\makecontent
	\phantom{.}
}

%-----------------------------------------------------------------------------
%		Content
%-----------------------------------------------------------------------------

\newcommand{\cvheadline}[1]{%
	\negative{\Large{}#1}%
}

\newcommand{\makecontent}{
	\setlength{\contenttextwidth}{\dimexpr(\paperwidth-\sidebarwidth-\contentleftmargin-\contentrightmargin)\relax}
	\begin{textblock*}{\contenttextwidth}(\contentabsoluteleftmargin, \contenttopmargin)
		\cvcontent
	\end{textblock*}
}

\newenvironment{longlist}{%
	\begin{tabular*}{\textwidth}{@{\extracolsep{\fill}}ll}
	}{%
	\end{tabular*}\vspace{\contentlistskip}
}

\newcommand{\longlistitem}[4]{%
	#1&\parbox[t]{0.83\textwidth}{%
		\textcolor{listitemcolor}{\large#2}%
		\hfill%
		{\small#3}\\%
		{\small#4}\vspace{\parsep}\vspace{\listitemskip}%
	}\\
}

\newenvironment{shortlist}{%
	\begin{tabular*}{\textwidth}{@{\extracolsep{\fill}}ll}
	}{%
	\end{tabular*}
}

\newcommand{\shortlistitem}[2]{%
	#1&\parbox[t]{0.83\textwidth}{%
		\textcolor{listitemcolor}{#2}%
	}\\
}

%-----------------------------------------------------------------------------
%		Sidebar
%-----------------------------------------------------------------------------

\newcommand{\makesidebarbackground}{
	\begin{tikzpicture}[remember picture,overlay]
		\draw [draw = sidebarcolor, fill=sidebarcolor] (current page.north west) rectangle ($(current page.south west) + (\sidebarwidth, 0cm)$);
	\end{tikzpicture}
}

\newcommand{\makeprofilepicture}{%
	\doifdef{\cvprofilepic}{%
		\begin{center}%
			\begin{tikzpicture}%
				\node[inner sep=0pt] at (0,0)
				{\includegraphics[width=\profilepicturewidth, height=\profilepicturemaxheight, keepaspectratio]{\cvprofilepic}};
			\end{tikzpicture}
		\end{center}
	}
}

\newcommand{\makename}{
	\doifdef{\cvfirstname}{
		{\Huge\negative{\cvfirstname}}
	}

	\doifdef{\cvlastname}{
		{\Huge\negative{\cvlastname}}
	}
}

\newcommand{\makepersonaldata}{
	\begin{tblr}{colspec={@{}Q[l,m]Q[l,m]},rows={\personaldataverticalspacing}}
			\textsc{\icon{Asterisk}} & \cvbirthdate\\
			\textsc{\icon{MapMarker}} & {\cvaddressa\\\cvaddressb}\\
			\textsc{\icon{Phone}} & \cvphonenumber\\
			\textsc{\icon{Envelope}} & \cvmail\\
	\end{tblr}
}

\newlength{\profilesectiontitlelength}
\newlength{\profilesectionlinelength}
\newcommand{\profilesectionhead}[1]{
	\settowidth{\profilesectiontitlelength}{\huge #1}
	\setlength{\profilesectionlinelength}{\dimexpr(\linewidth-\profilesectiontitlelength-2pt)\relax}
	{\color{profilesectiontitlecolor} \huge #1 \rule[0.15\baselineskip]{\profilesectionlinelength}{1pt}}%
}

\newcommand{\profilesection}[2]{
	\profilesectionhead{#1}
	
	#2
	
}
\newcommand{\makeprofilesecitons}{
	\cvprofile
}

\newcommand{\makesidebar}{
	\makesidebarbackground
	\begin{textblock*}{\sidebartextwidth}(\sidebarsidemargin, \sidebartopmargin)
		\makeprofilepicture
		
		\makename
		
		\makepersonaldata
		
		\makeprofilesecitons
	\end{textblock*}
}

%-----------------------------------------------------------------------------
%	Zweispaltige Extraseite
%-----------------------------------------------------------------------------

\setlength\columnsep{\extrapagecolumnsep}
\newcounter{extrapagecounter}
\newcommand{\extrapagetwocolumn}[3]{
	\setcounter{extrapagecounter}{0}
	{\Large\negative{#1}}\phantomsection\addcontentsline{toc}{subsection}{#1}
	
	#2
	
	\begin{multicols}{2}
		#3
	\end{multicols}
}

\newcommand{\extrapageentry}[2]{
	\addtocounter{extrapagecounter}{1}
	\textcolor{extrapageentrycolor}{\large\arabic{extrapagecounter} #1}
		
	#2
		
	
}





